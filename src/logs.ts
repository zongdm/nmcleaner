import chalk from 'chalk'
import dedent from 'dedent-js'

export const printHelp = (): void => {
	console.log(
		dedent(`${chalk.bgCyan(' HELP ')}
		First arg - path to 'node_modules'
		-r [RECURSIVE] - recursive remove
		`)
	)
}

export const printSuccessDelete = (folder: string[] | string): void => {
	console.log(
		dedent(`${chalk.bgGreen(' SUCCESSFUL DELETE ')}
		${Array.isArray(folder) ? folder.join('\n') : folder}
		`)
	)
}

export const printError = (msg: any): void => {
	console.log(`${chalk.bgRed(' ERROR ')}\n${msg}`)
}
