import { homedir } from 'os'
import { join, isAbsolute } from 'path'

interface IArgs {
	help: boolean
	path: string
	recursive: boolean
}

export const getArgs = (args: string[]): IArgs => {
	const result: IArgs = {
		help: false,
		path: '',
		recursive: false,
	}
	const [executer, file, ...rest] = args

	rest.forEach((value, index, array) => {
		if (index === 0) {
			if (value === '-h' || value === '--help') {
				result.help = true
			} else {
				result.path = isAbsolute(value) ? join(homedir(), value) : join(process.cwd(), value)
			}
			return
		}
		if (value.charAt(0) == '-') {
			if (value.substring(1) === 'r') result.recursive = true
		}
	})
	return result
}
