#! /usr/bin/env node
import { getArgs } from './args'
import { deleteFolder } from './delete'
import { printError, printHelp, printSuccessDelete } from './logs'

const deleteNodeModules = async (path: string, recursive: boolean): Promise<void> => {
	try {
		const folders = await deleteFolder(path, recursive)
		printSuccessDelete(folders)
	} catch (err) {
		printError(err instanceof Error ? err.message : err)
	}
}

const init = async (): Promise<void> => {
	const args = getArgs(process.argv)
	if (args.help) {
		return printHelp()
	}
	return await deleteNodeModules(args.path, args.recursive)
}
init()
