import { readdir, rm } from 'fs/promises'
import { existsSync } from 'fs'
import { join } from 'path'

export const deleteFolder = async (path: string, recursive: boolean): Promise<string[] | string> => {
	if (!existsSync(path)) {
		throw new Error('Invalid path')
	}

	if (recursive) {
		const folders = await readdir(path)
		const result: string[] = []

		for (const folder of folders) {
			const folderPath = join(path, folder, 'node_modules')
			if (existsSync(folderPath)) {
				await rm(folderPath, { recursive: true, force: true })
				result.push(folderPath)
			}
		}
		return result
	} else {
		await rm(join(path, 'node_modules'), { recursive: true, force: true })
		return join(path, 'node_modules')
	}
}
